module MyWorkSpace

    
    class WorkSpace

        def initialize(name:, parent: NilWorkSpace.new)
            @name = name
            @parent = parent
        end

        def path
           if @parent.is_a? NilWorkSpace 
               return File.join("/", @name)
           else
               return File.join(@parent.path, @name)
           end
        end

    end

    private 
    
    class NilWorkSpace
    end

end
