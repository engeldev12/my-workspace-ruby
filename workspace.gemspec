Gem::Specification.new do |spec|
    spec.name = "my_workspace"
    spec.version = ENV["CI_COMMIT_TAG"]? ENV['CI_COMMIT_TAG']: ENV.fetch("CI_JOB_ID", "1.dev")
    spec.summary = "For creation from my development workspace"
    spec.description = File.read("README.md")
    spec.authors = ["Engel J. Pint R."]
    spec.email = "engeljavierpinto@gmail.com"
    spec.files = ["lib/my_workspace.rb"]
    spec.homepage = ENV.fetch "CI_PROJECT_URL", "https://dev.com"
end
