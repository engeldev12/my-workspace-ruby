require "test_helper"


describe MyWorkSpace::WorkSpace do

    describe 'when i create a workspace without parent' do

        it 'path will be it same name' do
            
            @workspace = MyWorkSpace::WorkSpace.new name: "dev"
            _(@workspace.path).must_equal "/dev"

        end

    end

    describe 'when i create a workspace with it parent' do

        it 'path will be the join of the name of parent with it name' do

            @parent = MyWorkSpace::WorkSpace.new name: "engel"
            @workspace = MyWorkSpace::WorkSpace.new name: "dev", parent: @parent 

            _(@workspace.path).must_equal "/engel/dev"

        end

    end
end
